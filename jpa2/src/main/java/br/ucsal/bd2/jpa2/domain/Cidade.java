package br.ucsal.bd2.jpa2.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tab_cidade")
public class Cidade {

	@Id
	@Column(columnDefinition = "char(3)", nullable = false)
	private String sigla;// - char(3) - not null

	@Column(name = "nome", columnDefinition = "varchar(40)", nullable = false)
	private String nome; // - varchar(40) - not null

	@ManyToOne
	@JoinColumn(name = "sigla_estado", columnDefinition = "char(2)", nullable = false, foreignKey = @ForeignKey(name = "fk_cidade_estado"))
	private Estado estado;// - char(2) - not null

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Cidade(String sigla, String nome, Estado estado) {
		super();
		this.sigla = sigla;
		this.nome = nome;
		this.estado = estado;
	}

	public Cidade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Cidade [sigla=" + sigla + ", nome=" + nome + ", estado=" + estado + "]";
	}

}
