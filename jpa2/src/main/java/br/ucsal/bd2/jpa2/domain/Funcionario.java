package br.ucsal.bd2.jpa2.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "tab_funcionario", uniqueConstraints = {
		@UniqueConstraint(name = "uk_rg_rgOrgaoExpedidor_rgUf", columnNames = { "rg", "rgOrgaoExpedidor", "rgUf" }) })
public class Funcionario {

	@Id
	@Column(columnDefinition = "char(11)", nullable = false)
	private String cpf; // char(11) - not null

	@Column(columnDefinition = "varchar(40)", nullable = false)
	private String nome;// varchar(40) - not null

	@Column(columnDefinition = "char(12)", nullable = true)
	private String rg; // char(12) - null

	@Column(name = "rg_orgao_expedidor", columnDefinition = "varchar(20)", nullable = true)
	private String rgOrgaoExpedidor; // varchar(20) - null

	@Column(name = "rg_uf", columnDefinition = "char(2)", nullable = true)
	private String rgUf; // char(2) - null

	@ElementCollection
//	@CollectionTable(name = "tab_func_tels", joinColumns = @JoinColumn(name = "cpf_func", columnDefinition = "varchar(12)", foreignKey = @ForeignKey(name="fk_fun_tel")))
	@Column(columnDefinition = "varchar(12)")
	private List<String> telefones; // cada item varchar(12)

	@Temporal(TemporalType.DATE)
	@Column(name = "data_nascimento", nullable = false)
	private Date dataNascimento; // date - not null

	@Embedded
	@Column(nullable = false)
	private Endereco endereço;// embedded - not null

	public Funcionario() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getRgOrgaoExpedidor() {
		return rgOrgaoExpedidor;
	}

	public void setRgOrgaoExpedidor(String rgOrgaoExpedidor) {
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
	}

	public String getRgUf() {
		return rgUf;
	}

	public void setRgUf(String rgUf) {
		this.rgUf = rgUf;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEndereço() {
		return endereço;
	}

	public void setEndereço(Endereco endereço) {
		this.endereço = endereço;
	}

	public Funcionario(String cpf, String nome, String rg, String rgOrgaoExpedidor, String rgUf, List<String> telefones,
			Date dataNascimento, Endereco endereço) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.rg = rg;
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
		this.rgUf = rgUf;
		this.telefones = telefones;
		this.dataNascimento = dataNascimento;
		this.endereço = endereço;
	}

	@Override
	public String toString() {
		return "Funcionario [cpf=" + cpf + ", nome=" + nome + ", rg=" + rg + ", rgOrgaoExpedidor=" + rgOrgaoExpedidor
				+ ", rgUf=" + rgUf + ", telefones=" + telefones + ", dataNascimento=" + dataNascimento + ", endereço="
				+ endereço + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((dataNascimento == null) ? 0 : dataNascimento.hashCode());
		result = prime * result + ((endereço == null) ? 0 : endereço.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((rg == null) ? 0 : rg.hashCode());
		result = prime * result + ((rgOrgaoExpedidor == null) ? 0 : rgOrgaoExpedidor.hashCode());
		result = prime * result + ((rgUf == null) ? 0 : rgUf.hashCode());
		result = prime * result + ((telefones == null) ? 0 : telefones.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (dataNascimento == null) {
			if (other.dataNascimento != null)
				return false;
		} else if (!dataNascimento.equals(other.dataNascimento))
			return false;
		if (endereço == null) {
			if (other.endereço != null)
				return false;
		} else if (!endereço.equals(other.endereço))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (rg == null) {
			if (other.rg != null)
				return false;
		} else if (!rg.equals(other.rg))
			return false;
		if (rgOrgaoExpedidor == null) {
			if (other.rgOrgaoExpedidor != null)
				return false;
		} else if (!rgOrgaoExpedidor.equals(other.rgOrgaoExpedidor))
			return false;
		if (rgUf == null) {
			if (other.rgUf != null)
				return false;
		} else if (!rgUf.equals(other.rgUf))
			return false;
		if (telefones == null) {
			if (other.telefones != null)
				return false;
		} else if (!telefones.equals(other.telefones))
			return false;
		return true;
	}

}
