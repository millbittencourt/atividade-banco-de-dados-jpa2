package br.ucsal.bd2.jpa2.domain;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author jamille
 *
 */
@Entity
@Table(name = "tab_pessoa_juridica")
public class PessoaJuridica {

	@Id
	@Column(columnDefinition = "char(11)", nullable = false)
	private String cnpj;// - char(11) - not null

	@Column(columnDefinition = "varchar(40)", nullable = false)
	private String nome;// - varchar(40) - not null

	@Column(name = "ramos_atividade")
	@OneToMany(targetEntity = RamoAtividade.class)
	@CollectionTable(name = "tab_pjur_ramos_atv", foreignKey = @ForeignKey(name = "fk_ramos_cnpj"), joinColumns = @JoinColumn(name = "cnpj_pjur"), uniqueConstraints = {
			@UniqueConstraint(name = "uk_pjur_ramos_atv", columnNames = { "ramos_atv_id" }) })
	private List<RamoAtividade> ramosAtividade;

	@Column(columnDefinition = "numeric(10,2)", nullable = false)
	private Double faturamento;// - numérica(10,2) - not null

	@OneToMany(targetEntity = Vendedor.class)
	@CollectionTable(name = "tab_pjur_vend", foreignKey = @ForeignKey(name = "fk_cnpj_vend"), joinColumns = @JoinColumn(name = "cnpj_pjur"), uniqueConstraints = {
		@UniqueConstraint(name = "uk_pjur_vend", columnNames = { "cpf_vendedor" }) })
	private List<Vendedor> vendedores;

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<RamoAtividade> getRamosAtividade() {
		return ramosAtividade;
	}

	public void setRamosAtividade(List<RamoAtividade> ramosAtividade) {
		this.ramosAtividade = ramosAtividade;
	}

	public Double getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(Double faturamento) {
		this.faturamento = faturamento;
	}

	public List<Vendedor> getVendedores() {
		return vendedores;
	}

	public void setVendedores(List<Vendedor> vendedores) {
		this.vendedores = vendedores;
	}

	public PessoaJuridica() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PessoaJuridica(String cnpj, String nome, List<RamoAtividade> ramosAtividade, Double faturamento,
			List<Vendedor> vendedores) {
		super();
		this.cnpj = cnpj;
		this.nome = nome;
		this.ramosAtividade = ramosAtividade;
		this.faturamento = faturamento;
		this.vendedores = vendedores;
	}

	@Override
	public String toString() {
		return "PessoaJuridica [cnpj=" + cnpj + ", nome=" + nome + ", ramosAtividade=" + ramosAtividade
				+ ", faturamento=" + faturamento + ", vendedores=" + vendedores + "]";
	}

}
