package br.ucsal.bd2.jpa2.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.ucsal.bd2.jpa2.converter.SituacaoVendedorConverter;

@Entity
@Table(name = "tab_vendedor")
public class Vendedor extends Funcionario {

	@Column(name = "percentual_comissao", columnDefinition = "numeric(10, 2)", nullable = false)
	private Double percentualComissao;// - numeric(10,2) - not null

	@Column(name = "situacao_vendedor", columnDefinition = "char (3)", nullable = false)
	@Convert(converter = SituacaoVendedorConverter.class)
	private SituacaoVendedorEnum situacao;// - char(3) - not null

	@ManyToMany
	@JoinTable(name = "tab_pes_juridica_vendedor", joinColumns = @JoinColumn(name = "pjur_vend_cpf", referencedColumnName = "vend_cpf", foreignKey = @ForeignKey(name = "fk_pes_jur_vendedor")))
	private List<PessoaJuridica> clientes;

	public Double getPercentualComissao() {
		return percentualComissao;
	}

	public void setPercentualComissao(Double percentualComissao) {
		this.percentualComissao = percentualComissao;
	}

	public List<PessoaJuridica> getClientes() {
		return clientes;
	}

	public void setClientes(List<PessoaJuridica> clientes) {
		this.clientes = clientes;
	}

	public SituacaoVendedorEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoVendedorEnum situacao) {
		this.situacao = situacao;
	}

	public Vendedor(Double percentualComissao, SituacaoVendedorEnum situacao, List<PessoaJuridica> clientes) {
		super();
		this.percentualComissao = percentualComissao;
		this.situacao = situacao;
		this.clientes = clientes;
	}

	public Vendedor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Vendedor(String cpf, String nome, String rg, String rgOrgaoExpedidor, String rgUf, List<String> telefones,
			Date dataNascimento, Endereco endereço) {
		super(cpf, nome, rg, rgOrgaoExpedidor, rgUf, telefones, dataNascimento, endereço);
		// TODO Auto-generated constructor stub
	}

}
