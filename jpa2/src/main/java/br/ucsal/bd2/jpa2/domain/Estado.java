package br.ucsal.bd2.jpa2.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tab_estado")
public class Estado {

	@Id
	@Column(columnDefinition = "char(2)", nullable = false)
	private String sigla;// - char(2) - not null

	@Column(columnDefinition = "varchar(40", nullable = false)
	private String nome;// - varchar(40) - not null

	@Override
	public String toString() {
		return "Estado [sigla=" + sigla + ", nome=" + nome + "]";
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Estado(String sigla, String nome) {
		super();
		this.sigla = sigla;
		this.nome = nome;
	}

	public Estado() {
		super();
		// TODO Auto-generated constructor stub
	}

}
