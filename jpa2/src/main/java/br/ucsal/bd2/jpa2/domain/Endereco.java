package br.ucsal.bd2.jpa2.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author jamille
 *
 */
@Embeddable
public class Endereco {

	@Column(columnDefinition = "varchar(40)", nullable = false)
	private String logradouro;// - varchar(40) - not null

	@Column(columnDefinition = "varchar(40)", nullable = false)
	private String bairro;// - varchar(40) - not null

	@ManyToOne
	@JoinColumn(name = "sigla_cidade", columnDefinition = "char(3)", nullable = false, foreignKey = @ForeignKey(name = "fk_endereco_cidade"))
	private Cidade cidade; // char(3) - not null

	@Override
	public String toString() {
		return "Endereco [logradouro=" + logradouro + ", bairro=" + bairro + ", cidade=" + cidade + "]";
	}

	public Endereco(String logradouro, String bairro, Cidade cidade) {
		super();
		this.logradouro = logradouro;
		this.bairro = bairro;
		this.cidade = cidade;
	}

	public Endereco() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

}
