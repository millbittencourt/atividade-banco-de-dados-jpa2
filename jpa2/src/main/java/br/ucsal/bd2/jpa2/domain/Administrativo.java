package br.ucsal.bd2.jpa2.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tab_administrativo")
public class Administrativo extends Funcionario {

	@Column(nullable = false)
	private Integer turno;// - integer - not null

	public Integer getTurno() {
		return turno;
	}

	public void setTurno(Integer turno) {
		this.turno = turno;
	}

	public Administrativo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Administrativo(String cpf, String nome, String rg, String rgOrgaoExpedidor, String rgUf,
			List<String> telefones, Date dataNascimento, Endereco endereço) {
		super(cpf, nome, rg, rgOrgaoExpedidor, rgUf, telefones, dataNascimento, endereço);
		// TODO Auto-generated constructor stub
	}

	public Administrativo(Integer turno) {
		super();
		this.turno = turno;
	}

	@Override
	public String toString() {
		return "Administrativo [turno=" + turno + "]";
	}

}
