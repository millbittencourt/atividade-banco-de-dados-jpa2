package br.ucsal.bd2.jpa2.domain;

public enum SituacaoVendedorEnum {

	ATIVO("ATV"), SUSPENSO("SPN");

	private String codigo;

	private SituacaoVendedorEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public static SituacaoVendedorEnum valueOfCodigo(String codigo) {
		for (SituacaoVendedorEnum situacao : values()) {
			if (situacao.getCodigo().equalsIgnoreCase(codigo)) {
				return situacao;
			}
		}
		throw new IllegalArgumentException();
	}

}
